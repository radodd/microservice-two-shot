import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeForm from './Shoe_Form';
import ShoesList from './Shoes';
import HatList from './HatList';
import HatForm from './HatForm';

function App(props) {
  if (props.hats === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes" element={<ShoesList />}></Route>
          <Route path="shoes/new" element={<ShoeForm />} />
          <Route path="hats" element={<HatList hats={props.hats} />}></Route>
          <Route path="hats/new" element={<HatForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
