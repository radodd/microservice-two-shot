import React from 'react';
import { useEffect, useState} from 'react';

function ShoeFrom() {
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState("");
    const [model, setModel] = useState("");
    const [color, setColor] = useState("");
    const [picture, setPicture] = useState("");
    const [bin, setBin] = useState("");

    const handleManufacturer = event => {
        setManufacturer(event.target.value);
    }
    const handleModel = event => {
        setModel(event.target.value);
    }
    const handleColor = event => {
        setColor(event.target.value);
    }
    const handlePicture = event => {
        setPicture(event.target.value);
    }
    const handleBin = event => {
        setBin(event.target.value);
    }

    const handleSubmit = async event => {
        event.preventDefault();
        const data = {};
        data.manufacturer = manufacturer;
        data.model = model;
        data.color = color;
        data.picture_url = picture;
        data.bin = bin;

        const postURL = 'http://localhost:8080/api/shoes/';
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type" : "application/json",
            },
        };

        const shoeResponse = await fetch(postURL, fetchOptions);
        if (shoeResponse.ok){
            setManufacturer('');
            setModel('');
            setColor('');
            setPicture('');
            setBin('');
            // document.getElementById("create-shoe-form").reset(); value in jsx makes serValue(''); Work
        }
    }

    const fetchBins = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const binResponse = await fetch(url);
        if (binResponse.ok){
            const binData = await binResponse.json();
            setBins(binData.bins)
        }
    }

    useEffect(() => {
        fetchBins();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input value={manufacturer} onChange={handleManufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={model} onChange={handleModel} placeholder="Model" required type="text" name="model" id="model" className="form-control" />
                            <label htmlFor="model">Model</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={picture} onChange={handlePicture} required type="url" name="picture" id="picture" className="form-control" />
                            <label htmlFor="picture">Picture</label>
                        </div>
                        <div className="mb-3">
                            <select value={bin} onChange={handleBin} required name="bin" id="bin" className="form-select">
                                <option value="">Choose a bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option value={bin.href} key={bin.href}>
                                            {bin.closet_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ShoeFrom;
