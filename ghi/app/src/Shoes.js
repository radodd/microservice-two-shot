import { useEffect, useState} from 'react';

function ShoesList () {
    const [shoes, setShoes] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes'
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes)
            console.log(data)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = async (event) => {
        const shoeUrl = `http://localhost:8080/api/shoes/${event}/`
        const fetchConfig = {
            method: "delete",
        }

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            console.log(response.deleted, response.breakdown)
            fetchData();
        }
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Bin</th>
                    <th>Picture</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map((shoe) => {
                    return (
                        <tr className='fw-bold' key={shoe.id}>
                            <td className='fs-3'>{ shoe.manufacturer }</td>
                            <td className='fs-3'>{ shoe.model }</td>
                            <td className='fs-3'>{ shoe.color }</td>
                            <td>
                                <div className='fs-3'>{ shoe.bin.closet_name }</div>
                                <div>Section Number: {shoe.bin.bin_number}</div>
                                <div>Shelf Number: {shoe.bin.bin_size}</div>
                            </td>
                            <td><img className="img-thumbnail" height="200px" width="200px" src={ shoe.picture_url }/> </td>
                            <td>
                                <button onClick={() => handleDelete(shoe.id)} className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    )

}

export default ShoesList;
