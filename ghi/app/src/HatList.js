import { useEffect, useState } from 'react';

function HatList() {
    const [hats, setHats] = useState([]);

    const fetchData = async () => {
      const url = 'http://localhost:8090/api/hats'
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setHats(data.hats);
        console.log(data);
      }
    }
    useEffect(() => {
      fetchData();
    }, []);

    const handleDelete = async (event) => {
      const hatUrl = `http://localhost:8090/api/hats/${event}/`
      const fetchConfig = {
        method: "delete",
      }
      const response = await fetch(hatUrl, fetchConfig);
      if (response.ok) {
        console.log(response.deleted, response.breakdown)
        fetchData();
      }
    }

    return (
      <>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Fabric</th>
              <th>Style Name</th>
              <th>Color</th>
              <th>Location</th>
              <th>Picture</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody >
          {hats.map((hat) => {
            return (
              <tr key={hat.id}>
                <td className="fs-3">{ hat.fabric }</td>
                <td className="fs-3">{ hat.style_name }</td>
                <td className="fs-3">{ hat.color }</td>
                <td>
                  <div className="fs-3"> { hat.location.closet_name }</div>
                  <div>Section Number: { hat.location.section_number }</div>
                    <div>Shelf Number: {hat.location.shelf_number }</div>
                </td>
                <td><img className="img-thumbnail" height="200px" width="200px" src={ hat.picture_url }/></td>
                <td>
                  <button onClick={() => handleDelete(hat.id)} className="btn btn-danger">Delete</button>
                </td>
              </tr>
            )
          })}
          </tbody>
        </table>
      </>
    );
  }

  export default HatList;
