from django.db import models


class BinVO(models.Model):
    bin_number = models.PositiveSmallIntegerField()
    closet_name = models.CharField(max_length=100)
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.closet_name


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(max_length=200)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoe",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )
