from django.db import models

# Create your models here.
class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=100, unique=True)

class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style_name =models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(max_length=200, null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="+",
        on_delete=models.PROTECT,

    )
