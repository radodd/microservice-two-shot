# Wardrobify

Team:

* Ethan Flores - hats microservice
* Jorge Landeros - Shoe microservice

## Design

## Shoes microservice

Inside the Shoes microservice there are two models Shoe and BinVO. Shoe has 5 properties and one is a foreign key to bin, and BinVO is a value object to Bin inside the Wardrobe microservie and has all the properties including an extra one called import_href. I created a Shoe Poller that takes the already created bin inside the wardrobe microservice and creates a Value Object BinVO inside Shoes in order to reference Bin.

## Hats microservice

The Hats microservice are two models: Hat, LocationVO. Class Hat has 5 properties. 4 properties are given specificatoins and one which is a foreign key to named 'location' referencing 'LocationVO'. LocationVO has 4 properties. Three of the properties are referenced from Wardrob model and an additional property called import_href. This value object creates a table to store data from wardrobe model named Location. This data is accessed via poller of our design.
